.open assign.db
INSERT INTO accounts(id,balance) values(0,0);
begin transaction;
UPDATE accounts set balance 600 WHERE id=0;
UPDATE accounts set balance 0 WHERE id=2;
commit;

alter table accounts add column name string;
UPDATE accounts set name="Tom" WHERE id=3;
UPDATE accounts set name="Noam" WHERE id=2;
UPDATE accounts set name="Pit" WHERE id=1;
UPDATE accounts set name="Dan" WHERE id=0;

SELECT name as "ClientName",balance as "ClientBalance" FROM accounts;
